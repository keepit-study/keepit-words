import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from urllib.parse import urlparse, parse_qsl

from app import app
from apps import word, word1


app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname'), Input('url', 'href')])
def display_page(pathname, href):
    if pathname == '/word':
         return word.layout
    elif pathname == '/word1':
         return word1.layout
    else:
        return

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=80, processes=4)
