import re
import pickle
import pandas as pd
import numpy as np
import dash.dependencies as dd
import dash_core_components as dcc
import dash_html_components as html
from urllib.parse import urlparse, parse_qsl, urlencode
from dash.dependencies import Input, Output
import plotly.graph_objs as go

from app import app, df_counts

app.config.suppress_callback_exceptions = True

##########################
# loading Data & parameters
##########################

with open("replace_key.pkl", "rb") as f:
    param_keys = pickle.load(f)

param_keys = {k.lower():param_keys[k] for k in param_keys.keys()}

colors = {
    'background': '#F9F6EF',
    'text': '#0084E7'
}


component_ids = [
    'dropdown'
]


markdown_text = '''
### KEEP!T 블록체인 상식사전

KEEP!T 블록체인 상식사전에 연재되어 왔던 단어들의 출연 빈도를 볼수 있습니다.

데이터의 출처는 CoindDesk의 뉴스 기사들이며, 유사 단어들을 위한 사전처리를 하였습니다.

현재는 개발 버전으로, 단어가 없거나 기능이 없을 수 있습니다.

Feedback 은 언제나 환영합니다 sifnoc09@gmail.com.
'''



##########################
## Helper Functions
##########################

def apply_default_value(params):
    def wrapper(func):
        def apply_value(*args, **kwargs):
            if 'id' in kwargs and kwargs['id'] in params:
                kwargs['value'] = params[kwargs['id']]
            return func(*args, **kwargs)
        return apply_value
    return wrapper


def parse_state(url):
    parse_result = urlparse(url)
    params = parse_qsl(parse_result.query)
    state = dict(params)
    print(">>> state", state)
    return state


##########################
## Page Rendering Section
##########################

layout = html.Div([
    dcc.Location(id='url2', refresh=False),
    html.Div(id='page-layout2')
])


def build_layout(params):
    layout = html.Div(children=[
        html.H1(children=['Keep!T 블록체인 상식사전 | 단어 시각화'], style={'textAlign': 'center', 'color': colors['text']}),

        dcc.Markdown(children=markdown_text),

        apply_default_value(params)(dcc.Dropdown)(id='dropdown',
                     options=[{'label': k, 'value': k} for k in df_counts.columns], value=''),

        html.Div([
            html.Div(
                [html.Label("Time Span"),
                 dcc.Slider(
                     id='time-slider2',
                     updatemode='mouseup',
                     min=df_counts.index[0].timestamp(),
                     max=df_counts.index[-1].timestamp(),
                     marks={i.timestamp(): i.strftime("%Y") for i in df_counts.index},
                     value=df_counts.index[-1].timestamp(),
                 )], style={'width': '49%', 'float': 'right', 'marginBottom': 50}),

            html.Div([
                html.Label("누적일수 (기본 7일)"),
                dcc.Slider(
                    id='time-accum2',
                    updatemode='mouseup',
                    min=1,
                    max=31,
                    marks={i: 'Day {}'.format(i) if i == 1 else str(i) for i in range(1, 30)},
                    value=7,
                )], style={'width': '49%', 'marginBottom': 50, 'marginLeft': 20})], style={'marginTop': 15}),

        html.Div(id="my-div2"),
        dcc.Graph(id='graph-with-slider2'),
    ])

    return layout

##########################
## Call-Back Functions
##########################

@app.callback(Output('page-layout2', 'children'),
              inputs=[Input('url2', 'href')])
def page_load(href):
    if not href:
        return []
    state = parse_state(href)
    return build_layout(state)


@app.callback(Output('url2', 'search'),
              inputs=[Input(i, 'value') for i in component_ids])
def update_url_state(*values):
    state = urlencode(dict(zip(component_ids, values)))
    return f'?{state}'


@app.callback(
    dd.Output('my-div2', 'children'),
    [dd.Input('time-slider2', 'value'), dd.Input('dropdown', 'value')])
def update_info(end_time, sel_word): #todo sel_word 를 DataBase에서 통일 해줘야 한다.
    end_dt = pd.datetime.fromtimestamp(end_time).strftime("%Y-%m-%d")
    return "You selected `{word}` until {dt}. total counted : {count}".format(word = sel_word, dt = end_dt,
                                                                              count = df_counts[:end_dt][sel_word].sum())

@app.callback(
    dd.Output('graph-with-slider2', 'figure'),
    [dd.Input('time-slider2', 'value'),
     dd.Input('dropdown2', 'value'),
     dd.Input('time-accum2', 'value')])
def update_figure(end_time, sel_word, time_accum):
    filtered_df = df_counts[:pd.datetime.fromtimestamp(end_time)][sel_word].resample('{}D'.format(time_accum)).sum()
    traces = []
    traces.append(go.Scatter(
        #x=np.array(filtered_df_counts.reset_index().apply(lambda x: x['index'].strftime("%Y-%m-%d"), axis = 1)),
        y=filtered_df.values,
        text = np.array(filtered_df.reset_index().apply(lambda x: x['index'].strftime("%Y-%m-%d"), axis = 1)),
        marker={
            'size': 15,
            'line': {'width': 0.5, 'color': 'white'}
        },
        opacity=0.7,
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'type': 'log', 'title': 'Data Source from CoinDesk'},
            yaxis={'title': '단어 출현 빈도'},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            hovermode='closest'
        )
    }


if __name__ == '__main__':
    app.run_server(debug=True)