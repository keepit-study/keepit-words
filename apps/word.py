import numpy as np
import dash_core_components as dcc
import dash_html_components as html
from urllib.parse import urlparse, parse_qsl, urlencode
from dash.dependencies import Input, Output
import plotly.graph_objs as go

from app import app, df_counts, df_words, param_keys

app.config.suppress_callback_exceptions = True

##########################
# loading Data & parameters
##########################


colors = {
    'background': '#F9F6EF',
    'text': '#0084E7'
}


component_ids = [
    'dropdown'
]


markdown_text = '''
KEEP!T 블록체인 상식사전에 연재된 단어들의 출연 빈도를 볼수 있습니다.

데이터의 출처는 CoindDesk의 뉴스 기사들이며, 유사 단어들을 위한 사전처리를 하였습니다.

현재는 개발 버전으로, 단어가 없거나 기능이 없을 수 있습니다.

Feedback 은 언제나 환영합니다.

[sifnoc09@gmail.com](mailto:sifnoc09@gmail.com) 
'''


##########################
## Helper Functions
##########################

def apply_default_value(params):
    def wrapper(func):
        def apply_value(*args, **kwargs):
            if 'id' in kwargs and kwargs['id'] in params:
                kwargs['value'] = params[kwargs['id']]
            return func(*args, **kwargs)
        return apply_value
    return wrapper


def parse_state(url):
    parse_result = urlparse(url)
    params = parse_qsl(parse_result.query)
    state = dict(params)
    print(">>> state", state)
    return state

##########################
## Page Rendering Section
##########################
layout = html.Div([
    dcc.Location(id='url_loc', refresh=False),
    html.Div(id='page-layout')
])


def build_layout(params, markdown_text):
    layout = html.Div(className = 'row', children = [
        html.H1(children=['Keep!T 블록체인 상식사전 | 단어 시각화'], style={'textAlign': 'center', 'color': colors['text']}),

        html.Div(className = "five columns",
                 children = [dcc.Markdown(id='body_text', children=markdown_text)],
                 style={ 'border-radius': '1px',
                         'margin': '1px',
                         'marginBottom': 50,
                         'marginLeft': 10,
                         'marginRight': 15, 'marginTop': 15}),

        html.Div(className = "six columns",
                 children= [apply_default_value(params)(dcc.Dropdown)(id='dropdown',
                                                                      options=[{'label': param_keys[k], 'value': k.lower()} for k in df_counts.columns], value=''),
                            html.Div(id="my-div")
                            ], style={ 'border-radius': '1px',
                         'margin': '1px',
                         'marginTop': 15}),

        html.Div(className="six columns",
                 children=[html.Label("누적일수 (기본 7일)"),
                           dcc.Slider(
                               id='time-accum',
                               updatemode='mouseup',
                               min=1,
                               max=31,
                               marks={i: 'Day' if i == 1 else str(i) for i in range(1, 30)},
                               value=7,
             ),
            #dcc.Graph(id='graph-with-slider')
            ], style={ 'border-radius': '1px',
                         'margin': '3px',

                         'marginTop': 5}),

        html.Div(className="six columns",
                 children=[dcc.Graph(id='graph-with-slider')],
                 style={'border-radius': '1px',
                        'margin': '2px',
                        'marginTop': 15}),

        #This Div for storeing Selected Word and Word Contents.
        #TODO deprecate this, NOT USING FOR NOW
        html.Div(id='intermediate-value', style={'display': 'none'})
    ])

    return layout

##########################
## Call-Back Functions
##########################

@app.callback(Output('page-layout', 'children'),
              inputs=[Input('url_loc', 'href')])
def page_load(href):
    if not href:
        return []
    state = parse_state(href)
    return build_layout(state, markdown_text)


@app.callback(Output('url', 'search'),
              inputs=[Input(i, 'value') for i in component_ids])
def update_url_state(*values):
    state = urlencode(dict(zip(component_ids, values)))
    return f'?{state}'


@app.callback(
    Output('my-div', 'children'),
    [Input('dropdown', 'value')])
def update_info(sel_word): #todo sel_word 를 DataBase에서 통일 해줘야 한다.
    return "You selected `{word}`. total counted : {count}".format(word = param_keys[sel_word.upper()],
                                                                   count =df_counts[:][sel_word.upper()].sum())

@app.callback(
    Output('graph-with-slider', 'figure'),
    [Input('dropdown', 'value'),
     Input('time-accum', 'value')])
def update_figure(sel_word, time_accum):
    filtered_df = df_counts[:][sel_word.upper()].resample('{}D'.format(time_accum)).sum()
    traces = []
    traces.append(go.Scatter(
        x=filtered_df.index.to_datetime(),
        y=filtered_df.values,
        text = np.array(filtered_df.reset_index().apply(lambda x: x['index'].strftime("%Y-%m-%d"), axis = 1)),
        marker={
            'size': 15,
            'line': {'width': 0.5, 'color': 'white'}
        },
        opacity=0.7,
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'title': 'Data Source from CoinDesk'},
            yaxis={'title': '단어 출현 빈도'},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            hovermode='closest'
        )
    }


@app.callback(Output('body_text', 'children'), [Input('dropdown', 'value')])
def update_data(value):
    selected_word = value.upper()
    print(">>> selected word : ", selected_word)
    word_title = df_words[df_words['대체어'] == selected_word]['EPUB_단어'].values[0]
    word_content = df_words[df_words['대체어'] == selected_word]['EPUB_Description'].values[0]
    word_content_text = "### {} \n {}".format(word_title, word_content)
    return word_content_text


if __name__ == '__main__':
    app.run_server(debug=True)