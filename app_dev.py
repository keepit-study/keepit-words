import re
import dash
import dash.dependencies as dd
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

import numpy as np
import pandas as pd

df = pd.read_json("crypto_words_keepit.json")

def extract_eng(text):
    if '(' in text:
        return re.search('\\((.+)\\)', text).group(1).strip().replace(" ", "-").lower()
    else:
        return text.lower()

app = dash.Dash()

colors = {
    'background': '#F9F6EF',
    'text': '#0084E7'
}

markdown_text = '''
### KEEP!T 블록체인 상식사전

KEEP!T 블록체인 상식사전에 연재되어 왔던 단어들의 출연 빈도를 볼수 있습니다.

데이터의 출처는 CoindDesk의 뉴스 기사들이며, 유사 단어들을 위한 사전처리를 하였습니다.

현재는 개발 버전으로, 단어가 없거나 기능이 없을 수 있습니다.

Feedback 은 언제나 환영합니다 sifnoc09@gmail.com.
'''
app.config['suppress_callback_exceptions'] = True


word_layout = html.Div(
    [dcc.Location(id='url', refresh=False), html.Div(id='page-content')]
)

app.layout = html.Div(children=[
    html.H1(children=['Keep!T 블록체인 상식사전 | 단어 시각화'], style = {'textAlign': 'center', 'color':colors['text']}),

    dcc.Markdown(children=markdown_text),

    dcc.Dropdown(id='word_dropdown',
                 options=[{'label':k, 'value': extract_eng(k)} for k in df.columns]),

    html.Div([
    html.Div(
        [html.Label("Time Span"),
         dcc.Slider(
            id = 'time-slider',
            updatemode='mouseup',
            min=df.index[0].timestamp(),
            max=df.index[-1].timestamp(),
            marks={i.timestamp():i.strftime("%Y") for i in df.index},
            value=df.index[-1].timestamp(),
        )], style={'width': '49%', 'float':'right', 'marginBottom': 50}),

    html.Div([
        html.Label("누적일수 (기본 7일)"),
        dcc.Slider(
            id = 'time-accum',
            updatemode='mouseup',
            min=1,
            max=31,
            marks={i: 'Day {}'.format(i) if i == 1 else str(i) for i in range(1, 30)},
            value=7,
        )], style={'width': '49%', 'marginBottom': 50, 'marginLeft': 20})], style={'marginTop': 15}),

    html.Div(id ="my-div"),
    dcc.Graph(id='graph-with-slider'),
])

component_ids = [
    'dropdown'
]


@app.callback(Output('url', 'search'),
              inputs=[Input(i, 'value') for i in component_ids])
def update_url_state(*values):
    state = urlencode(dict(zip(component_ids, values)))
    return f'?{state}'

@app.callback(
    dd.Output('my-div', 'children'),
    [dd.Input('time-slider', 'value'), dd.Input('word_dropdown', 'value')])
def update_info(end_time, sel_word):
    end_dt = pd.datetime.fromtimestamp(end_time).strftime("%Y-%m-%d")
    return "You selected `{word}` until {dt}. total counted : {count}".format(word = sel_word, dt = end_dt,
                                                                              count = df[:end_dt][sel_word].sum())

@app.callback(
    dd.Output('graph-with-slider', 'figure'),
    [dd.Input('time-slider', 'value'),
     dd.Input('word_dropdown', 'value'),
     dd.Input('time-accum', 'value')])
def update_figure(end_time, sel_word, time_accum):
    filtered_df = df[:pd.datetime.fromtimestamp(end_time)][sel_word].resample('{}'.format(time_accum)).sum()
    traces = []
    traces.append(go.Scatter(
        #x=np.array(filtered_df.reset_index().apply(lambda x: x['index'].strftime("%Y-%m-%d"), axis = 1)),
        y=filtered_df.values,
        text = np.array(filtered_df.reset_index().apply(lambda x: x['index'].strftime("%Y-%m-%d"), axis = 1)),
        marker={
            'size': 15,
            'line': {'width': 0.5, 'color': 'white'}
        },
        opacity=0.7,
        ))

    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={'type': 'log', 'title': 'Data Source from CoinDesk'},
            yaxis={'title': '단어 출현 빈도'},
            margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
            hovermode='closest'
        )
    }

if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=8080)

