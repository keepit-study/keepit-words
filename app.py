import os
import pickle
import dash
from flask import Flask, send_from_directory, request, jsonify

import pandas as pd

server = Flask(__name__, static_folder='static')

class Dash_responsive(dash.Dash):
    """
    Overide Class for Change Title
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def index(self, *args, **kwargs):
        scripts = self._generate_scripts_html()
        css = self._generate_css_dist_html()
        config = self._generate_config_html()
        title = getattr(self, 'title', 'Keep!T Word')
        return ('''
        <!DOCTYPE html>
        <html>
            <head>
                <meta charset="UTF-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                <title>{}</title>
                {}
            </head>
            <body>
                <div id="react-entry-point">
                    <div class="_dash-loading">
                        Loading...
                    </div>
                </div>
            </body>
            <footer>
                {}
                {}
            </footer>
        </html>
        '''.format(title, css, config, scripts))

app = Dash_responsive(server=server)
app.css.append_css({'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'})
server = app.server

##################
## Load Datas
##################

df_counts = pd.read_json("crypto_words_keepit.json")
df_words = pd.read_csv("KEEPIT_WORD_TABLE.tsv", delimiter='\t')

with open("replace_key.pkl", "rb") as f:
    param_keys = pickle.load(f)

word_table = {k.lower():param_keys[k] for k in param_keys.keys()}
available_words = list(word_table.keys())

##################
### Routing
##################
@server.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(server.root_path, 'static'),
                               'favicon.ico')


@server.route('/api', methods = ['GET'])
def response():
    # get handling
    if request.method == 'GET':
        if 'word' in request.args:
            word = request.args['word']
            if word in available_words:
                word_key = word_table[word]
                data = df_counts[word.upper()][-365:]
                return data.to_json(orient="table")
        else:
            resp = jsonify({"msg":"No Match Word", "number":4})
            resp.status_code = 404
            return resp


app.config.suppress_callback_exceptions = False
